Se indicarán las consideraciones según el orden de implementación sugerido en el enunciado:

Parte 1
Implementado en ../userprog/syscall.c, haciendo uso de 'thread_current'

Parte 2
Se implementó según lo indicado en el enunciado. El código principal que permitió hacerlo está en ../userprog/process.c, en 'setup_stack' y en 'process_execute'. Se armó el stack según lo indicado en la documentación de pintos. Se insertan los argumentos de derecha a izquierda. Luego se insertan los punteros a los mismos en el mismo orden. Luego, se copia el número de argumentos. Finalmente la dirección de retorno. En cada paso indicado se decrementa la dirección (el stack se arma hacia abajo). Asi se arma el stack. Se usó la función 'strtok_r' y arreglos auxiliares para hacerlo, además del puntero al stack.

Parte 3
'halt': se llamó a la función 'shutdown_poweroff' en ../syscall.c

'exit': se imprime el id del proceso y el exit code, ubicado en el la memoria siguiente del stack de registro (en la variable 'p'), en ../syscall.c

'exec': se hace un semáforo. Se pide el primer argumento de la ejecución del proces, se hace un llamado para ejecutar el nuevo proceso ('process_execute') y luego retorna (cambiando el anterior 'break' por 'return').


