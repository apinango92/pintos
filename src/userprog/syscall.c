#include "userprog/syscall.h"
//#include "userprog/process.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/synch.h"



// Implementation Parameters
// =========================
static int WRITE_SIZE=128;

void get_arg (struct intr_frame *f, int *arg, int n);

inline
int
syscall_write(struct intr_frame *f)
{
  int *p = f->esp;  // Stack pointer (x86 registers http://www.scs.stanford.edu/05au-cs240c/lab/i386/s02_03.htm)

  //FIXME: This is (probably) displaced because of the -12 on process.c:442 (setup_stack)
  //int file_descriptor =         p[1];
  //char        *buffer = (char*) p[2];
  //int            size = (int)   p[3];  // huge size may cause overflow

  int file_descriptor =         p[5];
  char        *buffer = (char*) p[6];
  int            size = (int)   p[7];  // may overflow signed int!!

  switch(file_descriptor)
  {
    case STDIN_FILENO:
      // Inform that no data was written
      f->eax=0;
      // REVIEW: Should that process be terminated?
      return 2;

    case STDOUT_FILENO:
    {
      // Write in chunks of WRITE_SIZE
      //   putbuf (src/lib/kernel/console.c) locks the console,
      //   we should avoid locking it too often or for too long
      int remaining = size;
      while(remaining > WRITE_SIZE)
      {
        // Write a chunk
        putbuf(buffer, WRITE_SIZE);
        // Advance buffer pointer
        buffer    += WRITE_SIZE;
        remaining -= WRITE_SIZE;
      }
      // Write all the remaining data
      putbuf(buffer, remaining);

      // Inform the amount of data written
      f->eax=(int)size;
      return 0;
    }

    default:
      printf("syscall: write call not implemented for files\n");
      return 1;
  }

  return 1;  // Unreachable, but compiler complains
}

static void
syscall_handler (struct intr_frame *f)
{
  // intr_frame holds CPU register data
  //   Intel 80386 Reference Programmer's Manual (TL;DR)
  //     http://www.scs.stanford.edu/05au-cs240c/lab/i386/toc.htm

  int *p = f->esp;  // Stack pointer (x86 registers http://www.scs.stanford.edu/05au-cs240c/lab/i386/s02_03.htm)
  int syscall_number = (*p);
  struct thread* cur = thread_current();
  int arg[5];
  struct semaphore* s = (struct semaphore*) malloc(sizeof(struct semaphore));
  sema_init(s,1);
  switch(syscall_number)
  {
    case SYS_HALT:
      printf("system call: halt\n");
      shutdown_power_off();
      break;

    case SYS_EXIT:
      printf("%d: exit(%d)\n",cur->tid, (*(p+1)));
      break;

    case SYS_EXEC:
      sema_down(s);
      get_arg(f, &arg[0], 1);
      tid_t id = process_execute((char*)arg[0]);
      sema_up(s);
      printf("HIJO: %d\n",id);
      printf("PADRE: %d\n", cur->tid);
      f->eax = id;
      printf("system call: exec\n");
      return;

    case SYS_WAIT:
      printf("system call: wait\n");
      break;

    case SYS_WRITE:
      syscall_write(f);
      return;

    default:
      printf("system call: unhandled syscall. Terminating process[%d]\n",
             thread_current()->tid);
      break;
  }

  // Syscall handling failed, terminate the process
  thread_exit ();
}


void get_arg (struct intr_frame *f, int *arg, int n)
{
  int i;
  int *ptr;
  for (i = 0; i < n; i++)
    {
      ptr = (int *) f->esp + i + 1;
      arg[i] = *ptr;
    }
}



void
syscall_init (void)
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}
